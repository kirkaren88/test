package com.karen.kirakosyan;

import com.karen.kirakosyan.installer.model.Installer;
import com.karen.kirakosyan.installer.model.Progress;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter directory for installation!");
        String directory = scanner.nextLine();
         Installer installer = new Installer();
         installer.setDirectory(directory);
         installer.start();
         new Progress(installer).start();
    }
}
