package com.karen.kirakosyan.installer.model;

public class Progress extends Thread {
    Installer installer;

    public Progress(Installer installer) {
        this.installer = installer;
    }

    @Override
    public void run() {
        printProgress();
    }
    public synchronized void printProgress(){
        for (int i = 0; i < installer.getCount(); i++) {
            System.out.print("*");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            installer.notifyMe();
            synchronized (installer){ while (installer.getStatus()){
                try {
                    installer.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }}
        }
    }
}
