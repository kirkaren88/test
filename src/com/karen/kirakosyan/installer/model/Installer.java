package com.karen.kirakosyan.installer.model;

import java.io.*;

public class Installer extends Thread {

    private String directory;
    private  boolean status = false;
    private int count;
    private Thread installerThread;

    public Installer(){
        installerThread = new Thread();
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public boolean getStatus() {
        return status;
    }

    public int getCount() {
        return count;
    }

    public  void folderInstaller(File src, File dest) throws IOException, InterruptedException {
        if (src.isDirectory()) {
            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
                System.out.println("Directory copied from "
                        + src + "  to " + dest);
            }

            //list all the directory contents
            String files[] = src.list();
            count = files.length;

            for (String file : files) {
                //status = false;
             synchronized (this)  {
                 status = true;
                    //construct the src and dest file structure
                    File srcFile = new File(src, file);
                    File destFile = new File(dest, file);
                    //recursive copy

                    folderInstaller(srcFile, destFile);
                    sleep(1000);
                    status = false;
                    this.notify();
                    this.wait();
                    //status = false;
                    //Thread.sleep(1000);
                }
            }
        } else {

            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            //System.out.println("File copied from " + src + " to " + dest);
        }
    }

    public synchronized void notifyMe(){
        notify();
    }

    @Override
    public void run() {
        File srcFolder = new File("D:\\installer");
        File destFolder = new File(directory);
        //make sure source exists
        if (!srcFolder.exists()) {

            System.out.println("Directory does not exist.");
            //just exit
            System.exit(0);

        } else {

            try {
                folderInstaller(srcFolder, destFolder);
            } catch (IOException e) {
                e.printStackTrace();
                //error, just exit
                System.exit(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //System.out.println("Done");
    }
}
